package curso.hello;

abstract class Animal implements Nominavel {
	private String nome;
	
	Animal(String nome) {
		super();
		if (nome == null)
			throw new IllegalArgumentException();
		this.nome = nome;
	}
	
	public abstract String falar();

	public String getNome() {
		return nome;
	}

	@Override
	public String toString() {
		return String.format("%s [nome=%s]", getClass().getName(), getNome());
	}
}

class Gato extends Animal {
	Gato(String nome) {
		super(nome);
	}

	@Override
	public String falar() {
		return "O gato fez: Miau";
	}

	public String ronronar() {
		return "rrr-rrrr";
	}

}

class Pato extends Animal {
	Pato(String nome) {
		super(nome);
	}

	@Override
	public String falar() {
		return "O pato fez: Quack";
	}	
}

class Cao extends Animal {
	Cao(String nome) {
		super(nome);
	}

	@Override
	public String falar() {
		return "O cão fez: Au-au";
	}

}

public class MeuTerceiroPrograma {

	public static void main(String[] args) {
		Animal[] animais = { new Gato("Ronaldo"), new Pato("Donald"), 
				new Cao("Valdemir"), new Bovino("Marilu") };
		for (Object animal : animais) {
			System.out.println(animal);
		}
		for (Animal animal : animais) {
			System.out.println(animal.falar());
		}		
		for (Nominavel animal : animais) {
			System.out.println(animal.getNome());
		}
		for (Animal animal : animais) {
			System.out.println(animal.getNome());
		}		
		for (Animal animal : animais) {
			if (animal instanceof Gato) {
				Gato g = (Gato) animal;
				System.out.println(g.ronronar());
			}
		}	
		// System.out.println("O gato fez: Miau");
		// System.out.println("O pato fez: Quack");

		Object a = new Gato("Tom");
		if (a instanceof Gato) {
			Gato g = (Gato) a;
			System.out.println(g.ronronar());
		}

		Animal[] vet = new Animal[10];
		vet[0] = new Gato("Frajola");
		
		Object o = new Bovino("Ferdinando");
		
		//Animal a1 = new Animal();
		//a1.ronronar();

	}

}
