package curso.hello;

class Pessoa {
	String prefixo;
	String nome;	

	Pessoa(String nome) {
		this("", nome);
	}	
	
	Pessoa(String prefixo, String nome) {
		super();
		if (prefixo == null)
			prefixo = "";
		if (nome == null)
			throw new IllegalArgumentException("O nome não pode ser null!");
		if (nome.isBlank())
			throw new IllegalArgumentException("O nome não poder ficar em branco!");		
		this.prefixo = prefixo;
		this.nome = nome;
	}
	
	String getSaudacao() {
		return prefixo.isBlank()? nome : prefixo + " " + nome;
	}
	
	@Override
	public String toString() {
		return getSaudacao();
	}

}