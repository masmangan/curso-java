package curso.hello;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class MeuSegundoPrograma {
	public static void main(String[] args) {
		// Marco, Mário, Maria
		Pessoa[] pessoas = { new Pessoa("Marco"), new Pessoa("Dr.", "Mário"), new Pessoa("Senhora", "Maria"),
				new Pessoa("Mestra", "Márcia"), };
		List<Pessoa> lista = new ArrayList<>();
		for (Pessoa p : pessoas) {
			lista.add(p);
		}
		greet(lista);

		Map<Integer, Pessoa> mapa = new TreeMap<>();

		mapa.put(209, pessoas[0]);
		mapa.put(12, pessoas[1]);

		System.out.println(mapa.get(12));

	}

	private static void greet(Iterable<Pessoa> pessoas) {
		for (Pessoa p : pessoas) {
			System.out.printf("Olá, %s!\n", p.getSaudacao());
		}
	}
}
