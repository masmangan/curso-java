package curso.hello;

public class Bovino extends Animal {

	Bovino(String nome) {
		super(nome);
	}

	@Override
	public String falar() {
		return "O bovino fez: Muuuuuu--uu";
	}

	@Override
	public String toString() {
		return String.format("**Bovino** [nome=%s]", getNome());
	}

}
