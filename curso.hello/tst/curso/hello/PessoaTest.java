package curso.hello;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PessoaTest {

	@Test
	void testarQuePrefixoEstaVazioENomeEstaCorreto() {
		Pessoa p = new Pessoa("Marco");

		assertEquals("Marco", p.nome);
		assertEquals("", p.prefixo);
	}

	@Test
	void testarQuePrefixoENomeEstaoCorretos() {
		Pessoa p = new Pessoa("Dr.", "Marco");

		assertEquals("Marco", p.nome);
		assertEquals("Dr.", p.prefixo);
	}	

	@Test
	void testarQueNomeNaoPodeSerNull() {		
		assertThrows(IllegalArgumentException.class, () -> {
			new Pessoa(null); });
	}	

	@Test
	void testarQueNomeNaoPodeEstarEmBranco() {		
		assertThrows(IllegalArgumentException.class, () -> {
			new Pessoa(""); });
	}	
	
}
