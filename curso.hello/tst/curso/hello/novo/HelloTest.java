package curso.hello.novo;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
 
class HelloTest {

	@Test
	void testGreet() {
		String actual = Hello.greet();
		String expected = "Hello, World!";
		assertEquals(expected, actual);
	}

	@Test
	void testGreetAgain() {
		char actual = Hello.greet().charAt(0);
		char expected = 'H';
		assertEquals(expected, actual);
	}

}
